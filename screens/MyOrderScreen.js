import React, { Fragment } from "react";
import {Text, FlatList, ScrollView, View, TouchableOpacity, StyleSheet} from 'react-native';
import MyOrder from '../components/MyOrder';
import axios from "axios";
import MyContext from "../components/MyContext"
import AsyncStorage from '@react-native-community/async-storage';
import Icon from 'react-native-vector-icons/FontAwesome5';

export default class MyOrderScreen extends React.Component {
    static contextType = MyContext;

    static navigationOptions = ({ navigation }) => {
        return {
            headerTransparent: true,
            headerLeftContainerStyle:{
                position:'absolute',left:10,top:20,
            },
            headerLeft:(
                <TouchableOpacity activeOpacity={1}
                onPress={() => navigation.goBack()}
                style={{backgroundColor:'rgb(232,51,35)',borderRadius:50,width:50,height:50,
                zIndex:99,
                alignItems:'center'}}>
                    <Icon name="arrow-left" style={{lineHeight:50}} size={20} color={'white'} />                
                </TouchableOpacity>
            )
        }
    };

    constructor(props){
        super(props);

        this.state = {
            order: null,
        }
    }
    componentDidMount(){
        if(this.context.token === null){
            this.props.navigation.navigate('Auth');
        }
        else{
            this.props.navigation.addListener('willFocus', (route) => {
                this.getData()
            });            
        }
    }
    getData = () => {
        axios
            .get(`http://165.22.161.165/api/history`)
            .then(result => {
                    this.setState( prevState =>({
                        ...prevState,
                        order: result.data,
                    }));
                }
            )
            .catch((err) => {
                this.setState( prevState =>({
                    ...prevState,
                    order: null ,
                }));
            });
    }
    render() {
        if(this.context.token !==null){
            if(this.state.order){
                return (
                    <ScrollView style={{ backgroundColor:'rgba(83, 124, 189, 1)' }}>
                        <View style={{
                            paddingHorizontal:20,
                            paddingTop:20,
                            flexDirection:'row',
                            alingItems:'flex-end',
                            justifyContent:'flex-end',
                            marginBottom:40
                            }}>
                            <Text style={{textTransform:'uppercase',fontSize:20,color:'#ddd',letterSpacing:2,fontWeight:'bold'}}>
                                Вашы заказы
                            </Text>
                        </View>                        
                        <View style={{flex:1}}>
                        {this.state.order.map(item => (
                            <MyOrder {...item} key={item.id} />
                        )) }
                        </View>
                    </ScrollView>
                );
            }else{
                return this.context.getSpinner();
            }
        }else{
            return (
                <View style={styles.body}>
                    <View style={styles.noItem}>
                        <Text style={styles.noItemText}>Авторизуйтесь, чтобы видеть раздел</Text>
                    </View>
                </View>
            );
        }
    }
}
const styles = StyleSheet.create({
    body: {
        flex: 1,
    },
    noItem: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center'
    },
    noItemIcon: {
        width: 128,
        height: 128
    },
    noItemText: {
        marginTop: 16,
        fontSize: 18,
    },
});