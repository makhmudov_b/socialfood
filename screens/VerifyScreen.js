import React from "react";
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { TextInputMask } from 'react-native-masked-text';
import { Header } from "react-navigation";
import axios from "axios";
import MyContext from "../components/MyContext";
import { StackActions, NavigationActions } from 'react-navigation'
import AsyncStorage from '@react-native-community/async-storage';

export default class VerifyScreen extends React.Component {
    static contextType = MyContext;
    constructor(props){
        super(props);

        this.state={
            phone: null,
            password: null,
            user : null,
        }
        this._retrieveData();
    }
    static navigationOptions = {
        title: 'Вход',
    };

    componentDidUpdate(prevProps) {
        if(this.context.token){
            this.props.navigation.navigate('ProfileScreen');
        }else{

        }
    }

    _retrieveData = async () => {
        try {
          let _phone = await AsyncStorage.getItem('phone');
          this.setState({
              phone: _phone
          })
        } catch (error) {
        }
      };
    login = () =>{
            fetch("http://165.22.161.165/api/login",{
            method: 'POST',
            headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                phone: this.state.phone ,
                password: this.state.password
            })
        })
        .then(response => response.json())
        .then(
            (result) => {
                this.setState({
                user : result
            },
            this.check
            );
        })
        .catch(
            (error) => {
                console.warn(error)
        });
    }
    check = async () => {
        if(this.state.user !== null){
            await AsyncStorage.setItem('token', this.state.user.accessToken);
            this.context._bootstrapAsync();
            this.props.navigation.navigate('Home');
        }
        else{
            console.log('Error')
        }
    }
    onChangeText = (text) => {
        this.setState(prevState => ({
            ... prevState,
            password: text
        }))
    }
    render() {
        return (
            <View style={styles.body}>
                <Text style={styles.label}>Код из смс</Text>
                <View style={styles.inputBox}>
                <TextInputMask
                    returnKeyType={'done'}
                    keyboardType={'number-pad'} autoFocus={true} type={'custom'} style={[styles.input , {width:150}]}
                options={{ mask: '9999'}} value={this.state.password}
                onChangeText={(text) => this.onChangeText(text)} />
                </View>
                <TouchableOpacity style={styles.send} onPress={this.login}>
                    <Text style={styles.sendText}>Подтвердить</Text>
                </TouchableOpacity>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    body: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
        position: 'relative',
        backgroundColor: 'white'
    },
    send:{
        position: 'absolute',
        bottom: 0,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        textAlign: 'center',
        padding: 16,
        backgroundColor: 'blue'
    },
    sendText:{
        fontSize: 16,
        color: 'white',
        flex: 1,
        textAlign: 'center'
    },
    label:{
        textTransform: 'uppercase',
        fontSize: 16,
        marginVertical: 16,
        letterSpacing: 3
    },
    input:{
        fontSize: 22,
        letterSpacing: 20
    },
    inputBox:{
        display: 'flex',
        flexDirection: 'row',
        padding: 20,
        borderBottomWidth: 1,
        borderBottomColor: 'blue'
    },
    bottomText:{
        textAlign: 'center',
        marginTop: 36,
        padding: 24,
    }
});
