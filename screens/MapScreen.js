import React, { Fragment } from "react";
import { ScrollView, View, StyleSheet, Text, Platform, TouchableOpacity } from 'react-native';
import MapView from 'react-native-maps';
import MyContext from "../components/MyContext"
import RNAndroidLocationEnabler from 'react-native-android-location-enabler';
import AsyncStorage from "@react-native-community/async-storage";
import axios from "axios";
import Icon from 'react-native-vector-icons/FontAwesome5';

export default class MapScreen extends React.Component {
    static contextType = MyContext;
    static navigationOptions = ({ navigation }) => {
        return {
            headerTransparent: true,
            headerLeftContainerStyle:{
                position:'absolute',left:10,top:20,
            },
            headerLeft:(
                <TouchableOpacity activeOpacity={1}
                onPress={() => navigation.goBack()}
                style={{backgroundColor:'rgb(232,51,35)',borderRadius:50,width:50,height:50,
                zIndex:99,
                alignItems:'center'}}>
                    <Icon name="arrow-left" style={{lineHeight:50}} size={20} color={'white'} />                
                </TouchableOpacity>
            )
        }
    };

    constructor(props){
        super(props);

        this.state = {
            address: null,
            isLoaded: false,
            lat: null,
            long: null,
            region: null,
            initRegion: null,
        }
    }

    componentDidMount(){
        this.setState({
            initRegion: {
                latitude: this.context.cart.latitude,
                longitude: this.context.cart.longitude,
                latitudeDelta: 0.015,
                longitudeDelta: 0.0121,
            },
        })
    }
    // getCoordinates = async (region) => {
    //     let location = {
    //         long: region.longitude,
    //         lat: region.latitude,
    //     };
    //     axios.post("https://onlinetaom.uz/api/location",location)
    //         .then(result => {
    //             let data  = result.data.properties;
    //             let sub    = data.subLocality !== undefined ? data.subLocality : '';
    //             let name   =  data.streetName !== undefined ? data.streetName : '';
    //             let number = data.streetNumber !== undefined ? data.streetNumber : '';
    //             let address =  sub +" "+ name +" "+ number;
    //             this.setState({
    //                 address: address
    //             })
    //         })
    //         .catch((err) => {
    //         })
    //     }
    

    onSubmit = () => {
        let region = this.state.initRegion;
        this.context.postLocation(region.longitude,region.latitude);        
        this.context.locationChanged(true)
        this.props.navigation.goBack()
    };

    onRegionChange = async(region) => {
            this.setState({ initRegion : region });            
    }

    findMe = () => {
        if(Platform.OS ==='ios'){

            AsyncStorage.getItem('userLocation').then(location=>{
                let loc = JSON.parse(location);
                this.setState({
                    initRegion: {
                        latitude: loc.lat,
                        longitude: loc.long,
                        latitudeDelta: 0.015,
                        longitudeDelta: 0.0121,
                    },
                })
            })
        }else{
            RNAndroidLocationEnabler.promptForEnableLocationIfNeeded({interval: 10000, fastInterval: 5000})
                .then(data => {
                    this.context.getMyLocation().then(()=>{
                        AsyncStorage.getItem('userLocation').then(location=>{
                            let loc = JSON.parse(location);
                            this.setState({
                                initRegion: {
                                    latitude: loc.lat,
                                    longitude: loc.long,
                                    latitudeDelta: 0.015,
                                    longitudeDelta: 0.0121,
                                },
                            })
                        })
                    })
                }).catch(err => {
                    
            });
        }
    };

    render() {
        if(this.context.cart){
            return (
                <View style={styles.container}>
                    <MapView
                        showsUserLocation={true}
                        showsMyLocationButton={true}
                        style={styles.map}
                        region={this.state.initRegion}
                        initialRegion={this.state.initRegion}
                        onRegionChangeComplete={(region) => this.onRegionChange(region)}
                    >
                    </MapView>
                    <TouchableOpacity style={styles.icon}>
                        <Icon name="map-pin" size={40} />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.address}>
                        <Text>{this.state.address}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.confirm} onPress={this.onSubmit}>
                        <Text style={{color:'white'}}>
                            Подтвердить
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.findMe} onPress={this.findMe}>
                        <Icon style={styles.findMeIcon} name="compass" size={32} />
                    </TouchableOpacity>
                </View>
            );
        }
        else{
            return null;
        }
    }
}

const styles = StyleSheet.create({
    container: {
        ...StyleSheet.absoluteFillObject,
    },
    map: {
        display: 'flex',
        flexDirection: 'column',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    confirm : {
        alignSelf: 'center',
        textAlign: 'center',
        position: 'absolute',
        backgroundColor:'#0B2030',
        bottom: 20,
        padding: 20,
    },
    findMe : {
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        bottom: 20,
        right: 20,
        padding: 20,
    },
    address : {
        alignSelf: 'center',
        position: 'absolute',
        top: 30,
    },
    icon : {
        flex:1,
        justifyContent:'center',        
        alignSelf: 'center',
        position: 'absolute',
        top:'50%',
        left:'50%'
    }
});
