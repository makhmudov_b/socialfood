import React, { Component } from 'react'
import { Text, View , SafeAreaView, ScrollView, StatusBar , Image, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome5';
import Portion from '../components/Portion'
import Img from '../import/Img';
import MyContext from "../components/MyContext";
import axios from "axios";

export default class FoodScreen extends Component {
    static contextType = MyContext;

    static navigationOptions = ({ navigation }) => {
        return {
            headerTransparent: true,
            headerLeftContainerStyle:{
                position:'absolute',left:10,top:20,
            },
            headerLeft:(
                <TouchableOpacity activeOpacity={1}
                onPress={() => navigation.goBack()}
                style={{backgroundColor:'rgb(232,51,35)',borderRadius:50,width:50,height:50,
                zIndex:99,
                alignItems:'center'}}>
                    <Icon name="arrow-left" style={{lineHeight:50}} size={20} color={'white'} />                
                </TouchableOpacity>
            )
        }
    };    
    constructor(props){
        super(props);

        this.state = {
            isFav: false,
            favoriteId: 0,
            isLoaded: false,
            currentCat: 0,
            food: null,
            price: 0,
            size_id: 0,
            sizePrice: 0,
            params: [],
            paramsPrice: 0,
            amount: 1,
        }
    }
    componentDidMount() {
        this.getData();
    }
    getData = () => {
        const { navigation } = this.props;
        fetch(`http://165.22.161.165/api/restaurant/${navigation.getParam('alias')}/${navigation.getParam('id')}`)
            .then(res => res.json())
            .then((result) => {
                    if(result.size[0]){
                        this.setState({
                                isLoaded: true,
                                food: result,
                                price: result.price,
                                size_id: result.size[0].id,
                            }
                        );
                    }
                    else{
                        this.setState({
                                isLoaded: true,
                                food: result,
                                size_id: null,
                            }
                        );
                    }
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            );
    };
    sendPost = () => {
        if(this.context.token !==null) {
            if (this.state.food.restaurant.id === this.context.cart.restId || this.context.cart.restId===null || this.context.cart.restId===0) {
                var postData = {
                    amount: this.state.amount,
                    params: JSON.stringify(this.state.params),
                    size_id: this.state.size_id
                };
                axios.post(`http://165.22.161.165/api/order/${this.state.food.id}`, postData)
                    .then(result => {
                        this.context.updateCart(result.data.amount, result.data.price);
                        this.props.navigation.navigate('Cart')
                    })
                    .catch((err) => {
                        console.warn(err);
                    })
            } else {
                alert('Вы уже добавили еду из другого ресторана. Чтобы заказать с другого ресторана оформите заказ или очистите корзину');
            }
        }else{
            this.props.navigation.navigate('Profile')
        }
    };
    incrementAmount = () => {
        this.setState({
            amount : this.state.amount + 1,
        });
    }
    decrementAmount = () => {
        if(this.state.amount !== 1){
            this.setState({
                amount : this.state.amount - 1,
            });
        }
    }            
    render() {
        if(this.state.food){
        return (
            <SafeAreaView style={{ flex: 1 , backgroundColor:'rgba(226,230,239,0.3)' }}>
                <StatusBar backgroundColor="#0B2030" barStyle="light-content" />
                <View style={{ flex: 1 }}>
                    <ScrollView scrollEventThrottle={16} showsVerticalScrollIndicator={false}>
                        <View style={{position:'absolute',top:-50,left:0,right:0}}>
                            <Img name="back" width={'100%'} height={250} viewBox="0 0 350 160" />
                        </View>
                    <View style={{flex:1}}>
                        <View style={{paddingTop:90,paddingHorizontal:20,
                            flexDirection:'row',    
                            marginBottom:20,height:150}}>
                            <View style={{flex:1}}>
                            <Text style={{textTransform:'uppercase',fontSize:20,color:'#ddd',letterSpacing:2,fontWeight:'bold'}}>
                                Подробности
                            </Text>
                            </View>
                            <View style={{flex:1,alignItems:'flex-end'}}>
                                <Icon name="sliders-h" size={22} color={'#ddd'} />
                            </View>
                        </View>
                        <View style={{flex:1, justifyContent:'center',paddingHorizontal:15,position:'relative',top:-20 }}>
                            <View style={{flex:1,borderRadius:20,backgroundColor:'white',
                            paddingBottom: 30,
                            paddingHorizontal: 10,
                            alignItems:'center',
                            textAlign:'center',
                            shadowColor: "blue",
                            shadowOffset: {
                                width: 0,
                                height: 6,
                            },
                            shadowOpacity: 0.3,
                            shadowRadius: 7.5,
                            elevation: 12,
                            }}>
                                <Image source={{uri:this.state.food.image}} 
                                style={{resizeMode:'cover',
                                borderRadius:10,
                                marginTop: 10,
                                height:160,width:100+'%'}} />
                                <View style={{flex:1,flexDirection:'row',paddingHorizontal:10,
                                    paddingBottom:10,paddingTop:15
                                    }}>
                                    <Text style={{flex:1,textAlign:'left',fontWeight:'bold'}}>{this.state.food.name_ru}</Text>
                                    <Text style={{flex:1,textAlign:'right'}}>{this.state.food.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ")} сум</Text>
                                </View>
                                <Text style={{flex:1,paddingHorizontal:10,color:'rgba(0,0,0,.6)',lineHeight:22}}>
                                    {this.state.food.desc_ru}
                                </Text>
                                <View style={{flex:1,alignItems:'center',paddingHorizontal:10,
                                paddingTop:30,
                                paddingBottom:10,
                                textAlign:'left',flexDirection:'row'}}>
                                    <Text style={{flex:1,fontSize:20,textAlign:'left',fontWeight:'bold'}}>
                                        Рацион на весь день
                                    </Text>
                                </View>
                                {this.state.food ? this.state.food.params.map(item => (
                                    <Portion {...item} key={item.id} />
                                )) : null}
                            </View>
                        </View>
                    </View>                        
                    </ScrollView>
                    <View style={{position: 'absolute',
                                alignSelf: 'center',
                                alignItems:'center',
                                bottom: 20,left:0,right:0
                                }}>
                    <TouchableOpacity onPress={this.sendPost}
                     style={{backgroundColor:'rgb(232,51,35)',borderRadius:40,paddingVertical:20,width:150}}>
                        <Text style={{color:'white',fontWeight:'bold',textAlign:'center',textTransform:'uppercase'}}>Заказать</Text>
                    </TouchableOpacity>
                    </View>
                </View>
            </SafeAreaView>            
        )}
        return this.context.getSpinner()
    }
}
