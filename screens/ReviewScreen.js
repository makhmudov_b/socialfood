import React, { Component } from 'react'
import { Text, View , SafeAreaView, StatusBar, StyleSheet , Alert , TextInput, Image, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome5';
import ReviewItem from '../components/ReviewItem'
import Img from '../import/Img';
import MyContext from "../components/MyContext";
import axios from "axios";
import { Dropdown } from 'react-native-material-dropdown';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

export default class ReviewScreen extends Component {
    static contextType = MyContext;

    static navigationOptions = ({ navigation }) => {
        return {
            headerTransparent: true,
            headerLeftContainerStyle:{
                position:'absolute',left:10,top:20,
            },
            headerLeft:(
                <TouchableOpacity activeOpacity={1}
                onPress={() => navigation.goBack()}
                style={{backgroundColor:'rgb(232,51,35)',borderRadius:50,width:50,height:50,
                zIndex:99,
                alignItems:'center'}}>
                    <Icon name="arrow-left" style={{lineHeight:50}} size={20} color={'white'} />                
                </TouchableOpacity>
            )
        }
    };
    constructor(props){
        super(props);

        this.state = {
            distance: null,
            min: 5000,
            basket: null,
            data: [{
                value: 'Срочная',
            }],
            visibleSelector: false,
        }
    }
    componentDidMount(){
        this.updated();
        if(this.context.token !==null){
            this.props.navigation.addListener('didFocus', (route) => {
                setTimeout(this.updated,500);
            });
        }else{
            this.props.navigation.navigate('Profile');
        }
    }
    updated = async () => {
        axios
            .get(`http://165.22.161.165/api/order`)
            .then(result => {
                    this.setState(prevState => ({
                            ...prevState,
                            basket: result.data
                        })
                    );
                },
                (error) => {
                    console.warn(error)
                }
            );
    };
    setDate = date => {
        this.setState({
            chosenDate: date
        })
    };
    toPay = () => {
        const total = parseInt(this.context.cart.totalPrice);
        if(total>=this.state.min){
            this.props.navigation.navigate('Payment',{
                id: this.state.basket.id
            })
        }else{
            Alert.alert(
                'Ошибка',
                `Минимальная сумма заказа составляет: ${this.state.min} сум`,
                [
                    {text: 'Закрыть', onPress: () => console.log('OK Pressed')},
                ],
            );
        }
    };

    render() {
        if(this.state.basket){
            return (
            <SafeAreaView style={{ flex: 1 , backgroundColor:'#0B2030' }}>
                <StatusBar backgroundColor="#0B2030" barStyle="light-content" />
                    <KeyboardAwareScrollView behavior="padding">
                    <View style={styles.nameBox}>
                        <Text style={styles.name}>{this.state.basket ? this.state.basket.restaurant.name : 'Загрузка'}</Text>
                    </View>                        
                    <View style={styles.details}>
                            <Image source={{ uri: this.state.basket ? this.context.mainUrl+this.state.basket.restaurant.image : null}}
                                   style={styles.logo}/>
                            <View style={styles.distanceBox}>
                                <Text style={styles.distance}>{this.state.distance ? this.state.distance.distance : ''}</Text>
                            </View>
                        </View>
                        <View style={{
                        paddingBottom:30,
                        backgroundColor:'white',
                        justifyContent:'center',
                        borderTopLeftRadius: 60
                        }}>
                        <View style={styles.items}>
                            {this.state.basket ? this.state.basket.order_food.map(item => (
                                <ReviewItem {...item} key={item.id} updated={this.updated} />
                            )) : null}
                            <View style={styles.itemsDesc}>
                            </View>
                            <TouchableOpacity style={[styles.itemsDesc,{borderWidth:1,borderColor: 'red'}]} onPress={()=> this.props.navigation.navigate('Map')}>
                                <Text style={styles.itemsDescText}>
                                    {this.context.cart.address}
                                </Text>
                                <Icon name="marker" color={'blue'} style={styles.icon} size={20} />
                            </TouchableOpacity>                            
                            <Dropdown
                                containerStyle={styles.dropDownContainer}
                                label='Тип доставки'
                                value={this.state.basket.restaurant.categories[0].id == 1 ? "На следуюшее утро" :'Быстрая доставка'}
                            />
                            <TextInput
                                onChangeText={(text) => this.context.changeComment(text)}
                                placeholder={"Комментарий к заказу"}
                                style={styles.input}/>
                            <View style={styles.itemsDesc}>
                                <Text style={styles.itemsDescText}>Сумма заказа:</Text>
                                <Text style={styles.itemsDescPrice}>
                                    { parseInt(this.context.cart.totalPrice).toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") } сум</Text>
                            </View>
                            <View style={styles.itemsDesc}>
                                <Text style={styles.itemsDescText}>Сумма доставки:</Text>
                                <Text style={styles.itemsDescPrice}>{this.state.distance ? this.state.distance.price + ' сум' : 'Бесплатная'}</Text>
                            </View>
                            <View style={styles.itemsDesc}>
                                <Text style={styles.itemsDescText}>Итого:</Text>
                                <Text style={styles.itemsDescPrice}>
                                    { (parseInt(this.context.cart.totalPrice) +
                                    (this.context.cart.distance ? parseInt(this.context.cart.distance.price.replace(/\s/g,'')) : 0))
                                    .toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ")
                                    } сум</Text>
                            </View>
                                <View 
                                style={{
                                    flex:1,alignItems:'center',marginTop:30,
                                    flexDirection:'row',justifyContent:'center'
                                    }}>
                                    <TouchableOpacity style={styles.addCart} onPress={this.toPay}>
                                        <Text style={styles.addCartText}>Оплатить</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </KeyboardAwareScrollView>
                </SafeAreaView>
            );
        }else{
            return this.context.getSpinner();
        }
    }
}

const styles = StyleSheet.create({
    body: {
        position: 'relative',
        flex: 1,
    },
    details:{
        padding: 16,
        flexDirection: 'row',
        alignItems: 'flex-start',
    },
    noItem:{
        flexDirection: 'row',
        alignItems: 'center',
        textAlign: 'center'
    },
    nameBox:{
        paddingTop:30,
        justifyContent: 'center',
        alignItems: 'center'
    },
    distanceBox:{
        flex: 1,
        alignItems: 'flex-end',
        paddingVertical: 10
    },
    name:{
        flex:1,
        color:'white',
        flexDirection:'row',
        justifyContent:'center',
        textAlign:'center',
        textTransform: 'uppercase',
        letterSpacing: 2
    },
    cat:{
        color: 'rgba(51, 51, 51, 0.5)'
    },
    logo:{
        width: 50,
        height: 50,
    },
    priceBox:{
        textAlign: 'right',
        flexDirection: 'row'
    },
    price:{
        textAlign: 'right'
    },
    distance:{
        textAlign: 'right'
    },
    truckIcon:{
        marginRight: 8
    },
    items:{
        paddingVertical: 16,
        marginBottom: 10
    },
    itemsDesc:{
        marginHorizontal: 2,
        display: 'flex',
        flexDirection: 'row',
        padding: 16
    },
    itemsDescText:{
        fontSize: 16,
        flex: 1,
    },
    itemsDescPrice:{
        flex: 1,
        textAlign: 'right',
        fontSize: 16,
    },
    addCart:{
        width:200,
        backgroundColor:'#0B2030',
        borderTopLeftRadius:20,
        borderTopRightRadius:5,
        borderBottomLeftRadius:5,
        borderBottomRightRadius:10,        
        flexDirection: 'row',
        alignItems: 'center',
        textAlign: 'center',
        padding: 16,
    },
    addCartText:{
        fontSize: 16,
        flex: 1,
        color:'white',
        textAlign: 'center'
    },
    dropDownContainer:{
        marginHorizontal: 2,
        paddingHorizontal: 16,
    },
    input:{
        marginHorizontal: 2,
        padding: 16,
        borderBottomWidth: 1,
        borderBottomColor: '#EDEDED'
    },
});