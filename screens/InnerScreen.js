import React, { Component } from 'react'
import { Text, View , SafeAreaView, ScrollView, StatusBar , Image, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome5';
import Useful from '../components/Useful'
import Img from '../import/Img';

export default class InnerScreen extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
            headerTransparent: true,
            headerLeftContainerStyle:{
                position:'absolute',left:10,top:20,
            },
            headerLeft:(
                <TouchableOpacity activeOpacity={1}
                onPress={() => navigation.goBack()}
                style={{backgroundColor:'rgb(232,51,35)',borderRadius:50,width:50,height:50,
                zIndex:99,
                alignItems:'center'}}>
                    <Icon name="arrow-left" style={{lineHeight:50}} size={20} color={'white'} />                
                </TouchableOpacity>
            )
        }
    };    
    render() {
        return (
            <SafeAreaView style={{ flex: 1 , backgroundColor:'#192833', borderBottomWidth: 1, borderBottomColor:'rgba(0,0,0,.3)' }}>
                <StatusBar backgroundColor="#0B2030" barStyle="light-content" />
                <View style={{ flex: 1 }}>
                <ScrollView scrollEventThrottle={16} >                    
                <View style={{position:'absolute',top:-70,left:0,right:0}}>
                    <Image source={require('../assets/home.jpg')} style={{width:'100%',height:500}} />
                </View>
                    <View style={{flex:1}}>
                        <View style={{paddingTop:90,paddingHorizontal:20,
                            flexDirection:'row',
                            alignItems:'center',
                            marginBottom:40,height:150}}>
                        </View>
                        <View style={{flex:1,height:700,
                            shadowOffset: {
                                width: 0,
                                height: 8,
                            },
                            shadowOpacity: 1,
                            shadowRadius: 20,
                            elevation: 16,                            
                            marginHorizontal:10,
                            paddingHorizontal:20,
                            paddingVertical:20,
                            borderRadius:20,backgroundColor:'white'}}>
                            <Text style={{fontWeight:'bold',fontSize:22}}>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Numquam, impedit?</Text>
                            <Text style={{paddingTop:40,marginBottom:20,fontSize:18}}>
                                Lorem ipsum dolor, sit amet consectetur adipisicing elit. Commodi similique distinctio beatae. Modi, nesciunt blanditiis fugit rem cupiditate quaerat molestiae, at harum dolorum expedita cum adipisci. Vitae ipsa consequuntur nisi.
                            </Text>
                            <Image source={require('../assets/home.jpg')} style={{width:'100%',height:100}} />
                            <Text style={{paddingTop:40,fontSize:18}}>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Veritatis quod molestias magni nam officia recusandae voluptate aliquid voluptatem amet repellendus.
                            </Text>
                        </View>
                    </View>
                    </ScrollView>
                </View>
            </SafeAreaView>            
        )
    }
}
