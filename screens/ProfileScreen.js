import React, { Component } from 'react'
import { Text, View , SafeAreaView, ScrollView, StatusBar , Image, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome5';
import Img from '../import/Img';
import axios from "axios";
import MyContext from "../components/MyContext"
import AsyncStorage from '@react-native-community/async-storage';

export default class ProfileScreen extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
            headerTransparent: true,
        }
    };
    static contextType = MyContext;
    constructor(props) {
        super(props);

        this.state = {
            email: null,
            phone: null,
            name: null,
        };
    }
    componentDidMount() {
        this.props.navigation.addListener('willFocus', (route) => {
            this.getData();
        });
    }

    getData = () => {
        axios
            .get(`http://165.22.161.165/api/user`,
                {
                    headers: {'Authorization': `Bearer ${this.context.token} }`}
                })
            .then(result => {
                    this.setState({
                        email : result.data.email,
                        name : result.data.name,
                        phone: result.data.phone,
                    })},
                (error) => {
                    console.warn(error);
                }
            );
    }
    logout = async() => {
        axios.get(`http://165.22.161.165/api/logout`,
                {
                    headers: {'Authorization': `Bearer ${this.context.token} }`}
                })
            .then(result => {
                    AsyncStorage.clear();
                    this.context.logout();
                    this.context.changeCount(0);
                    this.props.navigation.navigate('Auth')
                },
                (error) => {
                    console.warn(error);
                }
            );
    }
    render() {
        return (
            <SafeAreaView style={{ flex: 1 , backgroundColor:'rgba(226,230,239,0.3)' }}>
                <StatusBar backgroundColor="#0B2030" barStyle="light-content" />
                <View style={{ flex: 1 }}>
                <View style={{position:'absolute',top:-70,left:0,right:0}}>
                    <Img name="back" width={'100%'} height={300} viewBox="0 0 350 160" />
                </View>
                    <View style={{flex:1}}>
                        <View style={{paddingTop:90,paddingHorizontal:20,
                            flexDirection:'row',
                            alignItems:'center',
                            marginBottom:20,height:150}}>
                            <View style={{flex:1}}>
                            <Text style={{textTransform:'uppercase',fontSize:20,color:'#ddd',letterSpacing:2,fontWeight:'bold'}}>
                                Профиль
                            </Text>
                            </View>
                            <View style={{flex:1,alignItems:'flex-end'}}>
                                <Icon name="user-circle" size={30} color={'#ddd'} />
                            </View>
                    </View>
                    <View style={{flex:1,marginTop:80}}>
                        <View style={{alignItems:'center',paddingBottom:20}}>
                            <Text style={{fontWeight:'bold',fontSize:22}}>+998900008991</Text>
                        </View>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('MyOrder') }>
                            <Text style={{textTransform:'uppercase',paddingVertical:20,paddingHorizontal:20,borderBottomColor:'rgba(0,0,0,0.2)',borderBottomWidth:2}}>Мои заказы</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.logout}>
                            <Text style={{textTransform:'uppercase',paddingVertical:20,paddingHorizontal:20,borderBottomColor:'rgba(0,0,0,0.2)',borderBottomWidth:2}}>Выйти</Text>
                        </TouchableOpacity>
                    </View>
                    </View>
                </View>
            </SafeAreaView>
        )
    }
}
