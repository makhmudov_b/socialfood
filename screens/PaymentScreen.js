import React, { Fragment } from "react";
import { StyleSheet, Text, View, Image, TouchableOpacity, Keyboard } from 'react-native';
import MyContext from "../components/MyContext";
import Icon from 'react-native-vector-icons/FontAwesome5';
import { NavigationEvents, NavigationActions, StackActions } from 'react-navigation';
import { WebView } from 'react-native-webview';
import AsyncStorage from "@react-native-community/async-storage";
import axios from "axios";

export default class PaymentScreen extends React.Component {
    static contextType = MyContext;

    static navigationOptions = ({ navigation }) => {
        return {
            headerTransparent: true,
            headerLeftContainerStyle:{
                position:'absolute',left:10,top:20,
            },
            headerLeft:(
                <TouchableOpacity activeOpacity={1}
                onPress={() => navigation.goBack()}
                style={{backgroundColor:'rgb(232,51,35)',borderRadius:50,width:50,height:50,
                zIndex:99,
                alignItems:'center'}}>
                    <Icon name="arrow-left" style={{lineHeight:50}} size={20} color={'white'} />                
                </TouchableOpacity>
            )
        }
    };

    constructor(props){
        super(props);

        this.state = {
            showWebView: false,
            paymeUrl: true,
            paid: true,
            payme: false,
            cash: false,
            keyboardSpace: 0
        };
    }

    renderContent() {
        return (
            <WebView
                source={{
                    uri: this.state.paymeUrl,
                }}
                onNavigationStateChange={this.onNavigationStateChange}
                startInLoadingState
                scalesPageToFit
                javaScriptEnabled
                style={{ flex: 1, marginBottom: this.state.keyboardSpace }}
            />
        );
    };

    onNavigationStateChange = navState => {
        if (navState.url.indexOf('https://onlinetaom.uz/') === 0) {
            this.goToOrder();
        }
    };

    componentDidMount() {
        Keyboard.addListener('keyboardDidShow',(frames)=>{
            this.setState({keyboardSpace: 200});
        });
        Keyboard.addListener('keyboardWillHide',(frames)=>{
            this.setState({keyboardSpace:0});
        });
    }

    checkout = async(method) => {
        let id = this.props.navigation.getParam('id');
        let location = await AsyncStorage.getItem('location');
        let destination = (JSON.parse(location));
        let {totalPrice, address, comment} = this.context.cart;
        let data = {
            payment_method: method,
            shipping_time : 'Срочный заказ',
            shipping_price : 0,
            distance : '0 км',
            adress : address,
            long : destination.long,
            lat : destination.lat,
            comment : 'comment',
            total_price : parseInt(totalPrice)
        };
        let res = await axios.post(`http://165.22.161.165/api/checkout/${id}`, data);
        if(res.data.url){
            this.setState({showWebView: true, paymeUrl: res.data.url},this.context.updateCart)
        }else{
            const resetAction = StackActions.reset({
                index: 0,
                actions: [
                    NavigationActions.navigate({ routeName: 'Main' }),
                ]
            });

            this.props.navigation.dispatch(resetAction);
            this.props.navigation.navigate('Main', {}, 
            NavigationActions.navigate({ routeName: 'Order', params: {id: id}}));
        }
    };

    goToOrder = () => {
        let id = this.props.navigation.getParam('id');
        const resetAction = StackActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({ routeName: 'Main' }),
            ]
        });

        this.props.navigation.dispatch(resetAction);
        this.props.navigation.navigate('Main', {}, NavigationActions.navigate({ routeName: 'Order', params: {id: id}}));
    };

    render() {
        return (
            <View style={styles.body}>
                <Text style={{ 
                color:'white',
                letterSpacing:4,
                textTransform:'uppercase',
                paddingBottom:40}}>Выберите способ оплаты</Text>
                { this.state.showWebView && this.renderContent() }
                {!this.state.showWebView ?
                <TouchableOpacity style={styles.payment}
                    onPress={()=>this.checkout(1)}>
                    <Image style={styles.paymentImage} source={require('../assets/payme.png')} />
                    <Text style={styles.paymentText}>Картой UzCard он-лайн</Text>
                </TouchableOpacity>
                :null}
                <TouchableOpacity style={styles.payment}
                    onPress={()=>this.checkout(0)}>
                    <Image style={styles.paymentImage} source={require('../assets/cash.png')} />
                    <Text style={styles.paymentText}>Кэш</Text>
                </TouchableOpacity>                
            </View>
        );
    }
}

const styles = StyleSheet.create({
    body: {
        flex: 1,
        backgroundColor:'#0B2030',
        justifyContent:'center',
        alignItems:'center',
        flexDirection: 'column'
    },
    payment:{
        width:  300,
        height: 260,
        backgroundColor:'white',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 20,
        elevation: 5,
        shadowColor: '#000000',
        shadowOffset: {
            width: 2,
            height: 2
        },
        shadowRadius: 2,
        shadowOpacity: 0.5
    },
    paymentImage:{
        width: 100,
        height: 100
    },
    paymentText:{
        fontSize: 18,
        marginTop: 16
    }
});
