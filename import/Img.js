/* eslint-disable */
import React from 'react';
import SvgIcon from 'react-native-svg-icon';
import svgs from './svgs';

const Img = (props) => <SvgIcon {...props} svgs={svgs} />;

export default Img;
