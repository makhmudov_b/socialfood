import React , {Component} from 'react';
import { StyleSheet, Text, View, Image, ActivityIndicator } from 'react-native';

import { createBottomTabNavigator , createAppContainer ,  createStackNavigator, createSwitchNavigator } from 'react-navigation'
import Icon from 'react-native-vector-icons/FontAwesome';
import HomeScreen from './screens/HomeScreen'
import CartScreen from './screens/CartScreen'
import FoodScreen from './screens/FoodScreen'
import ProfileScreen from './screens/ProfileScreen'
import AuthScreen from './screens/AuthScreen'
import VerifyScreen from './screens/VerifyScreen'
import AuthLoadingScreen from './screens/AuthLoadingScreen'
import UsefulScreen from './screens/UsefulScreen'
import InnerScreen from './screens/InnerScreen'
import OrderScreen from './screens/OrderScreen'
import RestaurantScreen from './screens/RestaurantScreen'
import ReviewScreen from './screens/ReviewScreen'
import PaymentScreen from './screens/PaymentScreen'
import MyOrderScreen from './screens/MyOrderScreen'
import MapScreen from './screens/MapScreen'
import MyContext from "./components/MyContext";
import CartButton from "./components/CartButton";
import RNAndroidLocationEnabler from "react-native-android-location-enabler";
import Geolocation from '@react-native-community/geolocation';
import axios from "axios";
import AsyncStorage from '@react-native-community/async-storage';


// if(__DEV__) {
//     import('./components/ReactotronConfig').then(() => console.warn('Reactotron Configured'))
// }

const spinner = StyleSheet.create({
  container: {
      flex: 1,
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center'
  },
  horizontal: {
      flexDirection: 'row',
      justifyContent: 'space-around',
      padding: 10
  },
  indicator:{
      flex: 1,
      justifyContent: 'space-around',
  }
});

const AuthStack = createStackNavigator({
  Auth: {
      screen: AuthScreen
  },
  Verify: {
      screen: VerifyScreen
  }
});

const ProfileStack = createStackNavigator({
  Profile: {
      screen: ProfileScreen
  },
  Order: {
      screen: OrderScreen
  },
  MyOrder: {
    screen: MyOrderScreen
    }  
});

const MainBottom = createBottomTabNavigator({
  Home: {
    screen: HomeScreen,
    navigationOptions: {
      tabBarLabel: 'Меню',
      tabBarIcon: ({ tintColor }) => (
        <Image source={require('./assets/social.png')} style={{ height: 28, width: 28, tintColor: tintColor }} />
      )
    }
  },
  Useful: {
    screen: UsefulScreen,
    navigationOptions: {
      tabBarLabel: 'Полезное',
      tabBarIcon: ({ tintColor }) => (
        <Icon name="heart" color={tintColor} size={26} />
      )
    }
  },
  Profile: {
    screen: createSwitchNavigator({
        AuthLoading: AuthLoadingScreen,
        ProfileScreen: ProfileStack,
        Auth: AuthStack,
    },
    {
        initialRouteName: 'AuthLoading'
    }),
    navigationOptions: {
      tabBarLabel: 'Профиль',
      tabBarIcon: ({ tintColor }) => (
        <Icon name="user-circle" color={tintColor} size={26} />
      )
    }
  },
  Cart: {
    screen: CartScreen,
    navigationOptions: {
      tabBarLabel: 'Корзина',
      tabBarIcon: ({ tintColor }) => (
          <CartButton tintColor={tintColor} />
      )
    }
  }
}, {
    tabBarOptions: {
      activeTintColor: '#537cbd',
      inactiveTintColor: '#ddd',
      style: {
        height:60,
        paddingBottom:5,
        backgroundColor: '#0B2030',
        borderTopWidth: 0,
        shadowOffset: { width: 5, height: 3 },
        shadowColor: 'black',
        shadowOpacity: 0.5,
        elevation: 5
      }
    }
  });
  const AppNavigator = createStackNavigator({
    Main: {
        screen: MainBottom,
        navigationOptions:{
          headerTransparent:true,
        }
    },
    Food: {
        screen: FoodScreen
    },
    Map: {
        screen: MapScreen
    },    
    Useful: {
      screen: UsefulScreen
    },
    UsefulInner:{
      screen: InnerScreen
    },
    Restaurant: {
      screen: RestaurantScreen
    },
    Review: {
        screen: ReviewScreen
    },
    Payment: {
        screen: PaymentScreen
    }
  },{
    mode: 'card',
  });
const AppContainer = createAppContainer(AppNavigator);

export default class App extends React.Component {
    static contextType = MyContext;
    state = {
        cart: {
            totalItems: 0,
            totalPrice: 0,
            latitude: null,
            longitude: null,
            address: null,
            isLoaded: false,
            restId: null,
            ready: false,
            payments: [],
            distance: null,
            time: null,
            comment:null
        },
        mainUrl: 'http://165.22.161.165/',
        token: null,
        locationChanged: false,
        paymeUrl: null,
    };

    componentDidMount = async() => {
        // this.checkToken();
        await this._bootstrapAsync();
        setTimeout(this.updateCart,200);

        if(Platform.OS==='ios'){
            await this.getMyLocation();
        }else{
            let data = await RNAndroidLocationEnabler.promptForEnableLocationIfNeeded({interval: 10000, fastInterval: 5000});
            await this.getAndroidLocation();
        }
        // await this.checkPermission();
        // await this.setToken();


        // this.removeNotificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen: NotificationOpen) => {
        //     // Get the action triggered by the notification being opened
        //     const action = notificationOpen.action;
        //     // Get information about the notification that was opened
        //     const notification: Notification = notificationOpen.notification;
        // });

        // this.removeNotificationDisplayedListener = firebase.notifications().onNotificationDisplayed((notification: Notification) => {
        // });
        // this.removeNotificationListener = firebase.notifications().onNotification((notification: Notification) => {
        // });
    };

    getAndroidLocation = async () => {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
                {
                    title: 'Внимание',
                    message:
                        'Разрешите приложению Social Food доступ к вашей геолокации',
                    buttonNegative: 'Нет',
                    buttonPositive: 'Разрешить (Рекомендуется)',
                },
            );
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                this.getMyLocation();
            } else {
                this.getMyLocation();
            }
        } catch (err) {
            this.getMyLocation();
        }
    }


    // setToken = async () => {
    //     if(this.state.token){
    //         let fcmToken = await AsyncStorage.getItem('fcmToken');
    //         axios.patch(this.state.mainUrl+`/api/set-token`,{token:fcmToken}).then(res=>{console.log('res',res)}).catch(err=>console.log('err',err));
    //     }
    // };



    // //1
    // async checkPermission() {
    //     const enabled = await firebase.messaging().hasPermission();
    //     if (enabled) {
    //         this.getToken();
    //     } else {
    //         this.requestPermission();
    //     }
    // }

    // //3
    // async getToken() {
    //     let fcmToken = await AsyncStorage.getItem('fcmToken');
    //     if (!fcmToken) {
    //         fcmToken = await firebase.messaging().getToken();
    //         if (fcmToken) {
    //             // user has a device token
    //             await AsyncStorage.setItem('fcmToken', fcmToken);
    //         }
    //     }
    // }

    // //2
    // async requestPermission() {
    //     try {
    //         await firebase.messaging().requestPermission();
    //         // User has authorised
    //         this.getToken();
    //     } catch (error) {
    //         // User has rejected permissions
    //     }
    // }
    logout = () => {
        this.setState(prevState => {
            token: null
        })
    }
    postLocation = async (long , lat) => {
        let location = {
            long: long,
            lat: lat,
        };
        await AsyncStorage.setItem('location',JSON.stringify(location));
        axios.post("https://onlinetaom.uz/api/location",location)
            .then(result => {
                let data  = result.data.properties;
                let sub    = data.subLocality !== undefined ? data.subLocality : '';
                let name   =  data.streetName !== undefined ? data.streetName : '';
                let number = data.streetNumber !== undefined ? data.streetNumber : '';
                this.setState(prevState => ({
                    ...prevState,
                    cart: {
                        ...prevState.cart,
                        latitude: lat,
                        longitude: long,
                        address:  sub +" "+ name +" "+ number,
                        ready: true
                    }
                }));
            })
            .catch((err) => {
            })
    };

    _bootstrapAsync = async () => {
        let token = await AsyncStorage.getItem('token');
        this.setState(prevState => ({
            ...prevState,
            token: token
        }));
        if(token !== null){
            axios.defaults.headers.common['Authorization'] = `Bearer ${token} }`;
        }
    };

    getSpinner = (logo=false) =>{
        if(logo){
            return (
                <View style={spinner.container}>
                    <ActivityIndicator size="large" style={spinner.indicator} color={'black'} />
                </View>
            )
        }else{
            return (
                <View style={[spinner.container, spinner.horizontal]}>
                    <ActivityIndicator size="large" style={spinner.indicator} color={'black'} />
                </View>
            )
        }
    };

    updateCart = () => {
        axios
            .get(`http://165.22.161.165/api/basket`)
            .then(result => {
                    this.setState(prevState => ({
                        ...prevState,
                        cart: {
                            ...prevState.cart,
                            totalItems: parseInt(result.data.amount , 10),
                            totalPrice: parseInt(result.data.price , 10),
                            restId: parseInt(result.data.restId , 10),
                            payments: JSON.parse(result.data.payments),
                        }
                    }));
                }
            )
            .catch((err) => {
                //console.warn(err);
            });
    };

    getMyLocation = async() => {
        let options = {
            enableHighAccuracy: true,
            timeout: 10000,
            maximumAge: 0
        };
        await Geolocation.getCurrentPosition((position) => {
                if(position.coords.longitude && position.coords.latitude){
                    this.postLocation(position.coords.longitude, position.coords.latitude).then(()=>{
                        let location = {
                            long: position.coords.longitude,
                            lat: position.coords.latitude
                        };
                        AsyncStorage.setItem('location',JSON.stringify(location));
                        AsyncStorage.setItem('userLocation',JSON.stringify(location));
                    });
                }
            },
            (err) => {
                console.warn(err);
                let location = {
                    long: 41.306788,
                    lat: 69.2723333
                };
                this.postLocation(69.2723333, 41.306788);
                AsyncStorage.setItem('location',JSON.stringify(location));

            }, options);
    };

    changeCount = (cnt) =>{
        this.setState(prevState => ({
            ...prevState,
            cart: {
                ...prevState.cart,
                totalItems: cnt,
            }
        }));
    };

    changeComment = (comment) =>{
        this.setState(prevState => ({
            ...prevState,
            cart: {
                ...prevState.cart,
                comment: comment,
            }
        }));
    };

    changeDistance = (distance) =>{
        this.setState(prevState => ({
            ...prevState,
            cart: {
                ...prevState.cart,
                distance: distance,
            }
        }));
    };

    locationChanged = (state) => {
        this.setState(prevState => ({
            ...prevState,
            locationChanged: state
        }));
    };

    render() {
        if (this.state.cart.address){
            return (
                <MyContext.Provider value={{
                    cart: this.state.cart,
                    updateCart : this.updateCart,
                    checkout : this.checkout,
                    postLocation : this.postLocation,
                    changeCount : this.changeCount,
                    getMyLocation : this.getMyLocation,
                    changeComment : this.changeComment,
                    changeDistance : this.changeDistance,
                    locationChanged : this.locationChanged,
                    locationChangedState : this.state.locationChanged,
                    getSpinner : this.getSpinner,
                    _bootstrapAsync : this._bootstrapAsync,
                    mainUrl : this.state.mainUrl,
                    paymeUrl : this.state.paymeUrl,
                    token : this.state.token
                    }} >
                    <AppContainer />
                </MyContext.Provider>
            );
        } else{
            return this.getSpinner(true);
        }
    }
}
