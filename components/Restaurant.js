import React, { Component } from 'react'
import { Text, View , Image , TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'

export default class Restaurant extends Component {
    render() {
        return (
            <TouchableOpacity
            activeOpacity={1}
            onPress={() => this.props.navigation.navigate('Restaurant',{
                alias: this.props.alias,
            })}>
                <View style={{
                    width: this.props.width - 20,
                    backgroundColor:'white',
                    borderRadius: 20,
                    justifyContent:'space-around',
                    shadowColor: "blue",
                    shadowOffset: {
                        width: 0,
                        height: 8,
                    },
                    shadowOpacity: 0.3,
                    shadowRadius: 10.5,
                    elevation: 16,
                    marginBottom:50,
                    paddingVertical:20,
                    paddingHorizontal:10
                    }}>
                    <View style={{flex:1,
                    position:'relative',
                    shadowOpacity: 0.3,
                    shadowRadius: 10,
                    elevation: 5,
                    position:'absolute',
                    top: 10,
                    right: 10,
                    borderRadius:100,
                    height:140,
                    width:140,
                    alignItems:'center',
                    justifyContent:'center',
                    backgroundColor:'white',
                    }}>
                        <Image
                        source={{uri:this.props.image , width:'100%',height:'100%'}}
                        style={{
                        borderRadius: 40,
                        flex:1,
                        resizeMode:'contain'}} />
                    </View>
                    <View style={{flex:1,alignItems:'flex-start'}}>
                        <Text style={{fontSize:10,paddingLeft:10,color:'gold',fontWeight:'bold'}}>{this.props.categories.length>0?this.props.categories[0].name_ru:""}</Text>
                        <Text style={{fontSize:20,paddingLeft:10,fontWeight:'bold'}}>{this.props.name}</Text>
                        <Text style={{fontSize:12,paddingLeft:10,minHeight:80,color:'#dddddd',maxWidth:160}}>{this.props.desc_ru}</Text>
                    </View>
                    <View style={{flex:1,alignItems:'flex-start',
                    marginTop:20, flexDirection:'row',
                    backgroundColor:'rgba(0,0,0,.05)',padding:15,borderRadius:10,alignItems:'center'}}>
                        <View style={{flex:3,flexDirection:'row'}}>
                            <Icon name="ios-clock" style={{marginRight:5}} size={20} color="rgba(0,0,0,.2)" />
                            <Text style={{color:'rgba(0,0,0,.5)'}}>
                                2000 колорий
                            </Text>
                        </View>
                        <View style={{flex:1,backgroundColor:'rgb(232,51,35)',
                        paddingVertical:8,paddingHorizontal:3,borderRadius:10}}>
                            <Text style={{textAlign:'center',fontSize:10,fontWeight:'bold',textTransform:'uppercase',color:'white'}}>
                                Подробнее
                            </Text>
                        </View>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }
}
