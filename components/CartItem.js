import React, { Component } from 'react'
import { Text, View , Image ,TouchableOpacity ,StyleSheet } from 'react-native'
import MyContext from "./MyContext"
import axios from "axios";
import Icon from 'react-native-vector-icons/Ionicons';

export default class CartItem extends Component {
    static contextType = MyContext;
    constructor(props){
        super(props);
        this.state = {
            amount: 1,
        }
    }    
    componentDidMount(){
        this.setState({
            amount : this.props.amount
        });
    }

    incrementAmount = () => {
        this.setState({
            amount : this.state.amount + 1,
        },
        this.update
        );
    };

    decrementAmount = () => {
        if(this.state.amount !== 1){
            this.setState({
                amount : this.state.amount - 1,

            },
            this.update
            );
        }
        else{
            this.delete()
        }
    };

    update = () => {
        let axiosConfig = {
            headers: {
                'Authorization': `Bearer ${this.context.token} }`
            }
        };
        axios
            .patch(`http://165.22.161.165/api/order/${this.props.id}`,
            {'amount':this.state.amount}, axiosConfig)
            .then(result => {
                this.props.updated();
            })
            .catch((err) => {
                console.log(err);
            })
    };

    delete = () => {
        let axiosConfig = {
            headers: {
                'Authorization': `Bearer ${this.context.token} }`
            }
        };
        axios
            .delete(`http://165.22.161.165/api/order/${this.props.id}`, axiosConfig)
            .then(result => {
               this.props.updated();
            })
            .catch((err) => {
                console.log(err);
            })
    };    
    render() {
        return (
            <View style={{
                flex:1,
                alignItems:'center',
                marginVertical:8,
                paddingVertical:10,
                marginHorizontal:10,
                paddingLeft:5,
                paddingRight:5,                
                flexDirection:'row',
                borderBottomColor:'#ddd',
                borderBottomWidth:1
                }}>
                <View style={{}}>
                    <Image source={require('../assets/social.png')} 
                    style={{ width:70,tintColor:'lightgreen',borderRadius:5,height:50,resizeMode:'cover'}}/>
                </View>
                <View style={{flex:1 ,
                    justifyContent:'flex-start',
                    alignItems:'flex-start',paddingLeft:10}}>
                    <Text style={{flex:1,fontWeight:'bold'}}>
                        {this.props.food.name_ru}
                    </Text>
                    <Text style={{flex:1,fontSize:11}}>
                        {this.props.food.desc_ru}
                    </Text>
                </View>
                <View style={{flex:1,
                    alignContent:'center',
                    justifyContent:'center',
                    alignItems:'center'}}>
                    <View style={styles.counter}>
                        <TouchableOpacity 
                        style={{
                            width:30,
                            height:30,
                            justifyContent:'center',
                            alignItems:'center',
                            borderRadius:50,
                            backgroundColor:'lightgreen'
                            }} onPress={this.decrementAmount}>
                            <Icon color={'white'} name="ios-remove" size={32} />
                        </TouchableOpacity>
                        <Text style={styles.count}>{this.state.amount}</Text>
                        <TouchableOpacity 
                        style={{
                            width:30,
                            height:30,                            
                            justifyContent:'center',
                            alignItems:'center',
                            borderRadius:16,
                            backgroundColor:'lightgreen'
                            }} onPress={this.incrementAmount}>
                            <Icon color={'white'} name="ios-add" size={32} />
                        </TouchableOpacity>
                    </View>
                    <Text style={{flex:1,fontSize:12}}>{this.props.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ")} сум</Text>
                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    counter:{
        borderRadius: 16,
        marginBottom:10,
        backgroundColor: '#E9FDE2',
        alignSelf: 'center',
        justifyContent:'center',
        flexDirection: 'row'
    },
    count:{
        paddingTop:6,
        justifyContent:'center',
        alignItems:'center',
        fontSize: 12,
        paddingHorizontal: 8,
    }
});