import React, { Component } from 'react'
import { Text, View,Image } from 'react-native'

export default class Portion extends Component {
    render() {
        return (
            <View style={{
                flex:1,
                alignItems:'center',
                marginVertical:8,
                paddingVertical:10,
                paddingLeft:5,
                flexDirection:'row',
                marginHorizontal:10,
                justifyContent:'space-between',
                shadowColor: "blue",
                shadowOffset: {
                    width: 0,
                    height: 8,
                },
                shadowOpacity: 0.3,
                shadowRadius: 10,
                borderRadius: 6,
                backgroundColor:'white',
                elevation: 8,     
                }}>
                <View style={{flex:1}}>
                    <Image source={{uri:this.props.image}} 
                    style={{ width:100,borderRadius:5,height:80,resizeMode:'cover'}}/>
                </View>
                <View style={{flex:2}}>
                    <Text style={{fontWeight:'bold'}}>
                        {this.props.name}
                    </Text>
                    <Text>
                        {this.props.desc_ru}
                    </Text>
                </View>
            </View>
        )
    }
}
