import React, { Component } from 'react'
import { Text, View,Image,TouchableOpacity } from 'react-native'

export default class Portion extends Component {
    render() {
        return (
            <TouchableOpacity onPress={() => this.props.navigation.navigate('UsefulInner')}
            style={{marginHorizontal:5,marginTop:10,marginBottom:10,paddingHorizontal:10,borderRadius:10,backgroundColor:'white',height:140}}>
            <View style={{flex:1,alignItems:'center',flexDirection:'row'}}>
                <View style={{flex:1}}>
                    <Image source={require('../assets/home.jpg')} style={{width:120,resizeMode:'contain',borderRadius:10}} />
                </View>
                <View style={{flex:2,paddingLeft:20}}>
                    <Text style={{fontWeight:'bold'}}>
                        Title
                    </Text>                                        
                    <Text>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Explicabo, harum.
                    </Text>
                </View>                                    
            </View>
        </TouchableOpacity>            
        )
    }
}
