import React, { Component } from 'react'
import { Text, View, ScrollView } from 'react-native'
import Info from './Info'

export default class Razion extends Component {
    render() {
        return (
            <ScrollView horizontal={true}
            showsHorizontalScrollIndicator={false}>
                <Info color={'skyblue'} imageUri={require('../assets/food.png')} name="Завтрак" />
                <Info color={'lightgreen'} imageUri={require('../assets/food.png')} name="Ланч" />
                <Info color={'gold'} imageUri={require('../assets/food.png')} name="Обед" />
                <Info color={'orange'} imageUri={require('../assets/food.png')} name="Полдник" />
                <Info color={'skyblue'} imageUri={require('../assets/food.png')} name="Ужин" />
            </ScrollView>
        )
    }
}
